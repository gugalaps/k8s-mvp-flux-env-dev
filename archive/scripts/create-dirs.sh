#!/bin/sh

if [ ! -d ./vers ] 
then
    mkdir -p ./vers
fi
chmod 775 ./vers

if [ ! -d ./tmp ] 
then
    mkdir -p ./tmp
fi
chmod 775 ./tmp
