#!/bin/sh


namespace=$1
helm_release_name=$2

#images:
image_pg_version=$3
image_redis_version=$4
image_worker_version=$5
image_api_version=$6
image_app_version=$7
image_nginx_version=$8
image_ingress_version=$9


#manifests:
manifest_pg_version=${10}
manifest_redis_version=${11}
manifest_worker_version=${12}
manifest_api_version=${13}
manifest_app_version=${14}
manifest_nginx_version=${15}
manifest_ingress_version=${16}

fluxcd_automated=${17}


if [ ! -d ./environment/charts/$namespace ] 
then
    mkdir -p ./environment/charts/$namespace
fi
chmod 775 ./environment/charts/$namespace




####### CHART ########

echo "---
name: $namespace
version: 0.2.4
description: Flux
maintainers:
  - name: czm41k
    email: etselikov@express42.com
appVersion: 1.1
">./environment/charts/$namespace/Chart.yaml

chmod 775 ./environment/charts/$namespace/Chart.yaml

echo "---
  dependencies:
    - name: redis
      version: $manifest_redis_version
      repository: https://raw.githubusercontent.com/sim-repo/micro-service-app/master

    - name: pg
      version: $manifest_pg_version
      repository: https://raw.githubusercontent.com/sim-repo/micro-service-app/master

    - name: worker
      version: $manifest_worker_version
      repository: https://raw.githubusercontent.com/sim-repo/micro-service-app/master

    - name: api
      version: $manifest_api_version
      repository: https://raw.githubusercontent.com/sim-repo/micro-service-app/master
    
    - name: app
      version: $manifest_app_version
      repository: https://raw.githubusercontent.com/sim-repo/micro-service-app/master
    
    - name: nginx
      version: $manifest_nginx_version
      repository: https://raw.githubusercontent.com/sim-repo/micro-service-app/master
    
    - name: ingress
      version: $manifest_ingress_version
      repository: https://raw.githubusercontent.com/sim-repo/micro-service-app/master
">./environment/charts/$namespace/requirements.yaml

chmod 775 ./environment/charts/$namespace/requirements.yaml


####### NAMESPACE ########
echo "apiVersion: v1
kind: Namespace
metadata:
  labels:
    flux: enabled # custom label - need for cleanup job
  name: $namespace
">./environment/namespaces/$namespace.ns.yaml

chmod 775 ./environment/namespaces/$namespace.ns.yaml



source ./scripts/state-for-gen.sh
externalPort=$((externalPort+1))
echo "#!/bin/sh" > ./scripts/state-for-gen.sh
echo externalPort=$externalPort >> ./scripts/state-for-gen.sh

####### VALUES ########
echo "---
nginx:
  image:
    repository: neogeowild/nginx
    tag: latest
  namespace: $namespace
  externalPort: $externalPort

ingress:
  namespace: $namespace
  externalPort: $externalPort
">./environment/charts/$namespace/values.yaml

chmod 775 ./environment/charts/$namespace/values.yaml


####### HELM RELEASE ########
echo "---
  apiVersion: helm.fluxcd.io/v1
  kind: HelmRelease
  metadata:
    name: $helm_release_name
    namespace: $namespace
    annotations:
      fluxcd.io/ignore: \"false\"
      fluxcd.io/automated: \"$fluxcd_automated\"
      fluxcd.io/tag.pg: semver:~0.1
      fluxcd.io/tag.redis: semver:~0.1
      fluxcd.io/tag.worker: semver:~0.1
      fluxcd.io/tag.api: semver:~0.1
      fluxcd.io/tag.app: semver:~0.1
      fluxcd.io/tag.nginx: semver:~0.1
  spec:
    releaseName: $helm_release_name
    helmVersion: v3
    chart:
      git: git@gitlab.com:gugalaps/k8s-mvp-flux-env-dev
      ref: master
      path: environment/charts/$namespace
    values:
      pg:
        image:
          repository: postgres
          tag: $image_pg_version
        namespace: $namespace
      redis:
        image:
          repository: redis
          tag: $image_redis_version
        namespace: $namespace
      worker:
        image:
          repository: neogeowild/worker
          tag: $image_worker_version
        namespace: $namespace
      api:
        image:
          repository: neogeowild/api
          tag: $image_api_version
        namespace: $namespace
      app:
        image:
          repository: neogeowild/app
          tag: $image_app_version
        namespace: $namespace
      nginx:
        image:
          repository: neogeowild/nginx
          tag: $image_nginx_version
        namespace: $namespace
        externalPort: $externalPort
      ingress:
        namespace: $namespace
        externalPort: $externalPort">./environment/releases/$namespace.values.yaml

chmod 775 ./environment/releases/$namespace.values.yaml