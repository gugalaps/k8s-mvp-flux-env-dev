#!/bin/sh



for f in ./tmp/* ; do
    source $f
    cp $f ./vers/$namespace.sh
    echo '\n#updated: '$(date)>> ./vers/$namespace.sh
done

for f in ./tmp/* ; do
    rm $f
done

for f in ./vers/* ; do
    source $f

    if [ $command = "CREATE" ]; then
        ./scripts/gen-flux-values.sh $namespace \
        $helm_release_name $image_pg_version $image_redis_version $image_worker_version $image_api_version $image_app_version $image_nginx_version $image_ingress_version $manifest_pg_ver $manifest_redis_ver $manifest_worker_ver $manifest_api_ver $manifest_app_ver $manifest_nginx_ver $manifest_ingress_ver $fluxcd_automated
    fi

    if [ $command = "DELETE" ]; then
        ./scripts/cleanup.sh $namespace
    fi
done