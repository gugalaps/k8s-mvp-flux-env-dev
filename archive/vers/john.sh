#!/bin/sh
namespace=john
helm_release_name=john
#images:
image_pg_version=latest
image_redis_version=latest
image_worker_version="0.1.0"
image_api_version="0.1.0"
image_app_version="0.1.0"
image_nginx_version="0.1.2"
image_ingress_version="0.1.0"
#manifests:
manifest_pg_ver="0.1.0"
manifest_redis_ver="0.1.0"
manifest_worker_ver="0.1.0"
manifest_api_ver="0.1.0"
manifest_app_ver="0.1.0"
manifest_nginx_ver="0.1.0"
manifest_ingress_ver="0.1.0"
#fluxcd.automated: true/false
fluxcd_automated="false"
#command: CREATE/DELETE
command=CREATE


#updated: пятница, 1 мая 2020 г. 16:49:32 (MSK)
