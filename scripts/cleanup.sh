#!/bin/sh

namespace=$1

rm ./vers/$namespace.sh
rm ./environment/releases/$namespace.values.yaml
rm ./environment/namespaces/$namespace.ns.yaml
rm -r ./environment/charts/$namespace