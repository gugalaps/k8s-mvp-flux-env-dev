#!/bin/sh

if [ ! -d ./vers ] 
then
    mkdir -p ./vers
fi
chmod 775 ./vers

if [ ! -d ./tmp ] 
then
    mkdir -p ./tmp
fi
chmod 775 ./tmp


if [ ! -d ./environment ] 
then
    mkdir -p ./environment
fi
chmod 775 ./environment


if [ ! -d ./environment/charts ] 
then
    mkdir -p ./environment/charts
fi
chmod 775 ./environment/charts


if [ ! -d ./environment/namespaces ] 
then
    mkdir -p ./environment/namespaces
fi
chmod 775 ./environment/namespaces


if [ ! -d ./environment/releases ] 
then
    mkdir -p ./environment/releases
fi
chmod 775 ./environment/releases