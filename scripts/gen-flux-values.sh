#!/bin/sh

namespace=$1
helm_release_name=$2

#images:
image_ad_version=$3
image_cart_version=$4
image_checkout_version=$5
image_currency_version=$6
image_email_version=$7
image_frontend_version=$8
image_loadgenerator_version=$9
image_payment_version=${10}
image_productcatalog_version=${11}
image_recommendation_version=${12}
image_shipping_version=${13}

#manifests:
manifest_ad_version=${14}
manifest_cart_version=${15}
manifest_checkout_version=${16}
manifest_currency_version=${17}
manifest_email_version=${18}
manifest_frontend_version=${19}
manifest_loadgenerator_version=${20}
manifest_payment_version=${21}
manifest_productcatalog_version=${22}
manifest_recommendation_version=${23}
manifest_shipping_version=${24}
manifest_grafana_load_dashboards_version=${25}
manifest_istio_vservices_version=${26}
manifest_flagger_version=${27}
fluxcd_automated=${28}

host_prefix=${29}
ingress_gateway_ip=${30}

if [ ! -d ./environment/charts/$namespace ] 
then
    mkdir -p ./environment/charts/$namespace
fi
chmod 775 ./environment/charts/$namespace



####### CHART ########

echo "---
name: $namespace
version: 0.2.4
description: Flux
maintainers:
  - name: czm41k
    email: etselikov@express42.com
appVersion: 1.1
">./environment/charts/$namespace/Chart.yaml

chmod 775 ./environment/charts/$namespace/Chart.yaml

echo "---
  dependencies:
    - name: ad
      version: $manifest_ad_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master

    - name: cart
      version: $manifest_cart_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master
    
    - name: checkout
      version: $manifest_checkout_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master

    - name: currency
      version: $manifest_currency_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master

    - name: email
      version: $manifest_email_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master
    
    - name: frontend
      version: $manifest_frontend_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master
    
    - name: loadgenerator
      version: $manifest_loadgenerator_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master
    
    - name: payment
      version: $manifest_payment_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master
    
    - name: productcatalog
      version: $manifest_productcatalog_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master

    - name: recommendation
      version: $manifest_recommendation_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master

    - name: shipping
      version: $manifest_shipping_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master

    - name: istio-vservices
      version: $manifest_istio_vservices_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master

    - name: flagger
      version: $manifest_flagger_version
      repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master
    
   # - name: grafana-load-dashboards
   #   version: $manifest_grafana_load_dashboards_version
   #   repository: https://raw.githubusercontent.com/sim-repo/k8s-mvp-src/master
      ">./environment/charts/$namespace/requirements.yaml

chmod 775 ./environment/charts/$namespace/requirements.yaml


####### NAMESPACE ########
echo "apiVersion: v1
kind: Namespace
metadata:
  labels:
    istio-injection: enabled
    flux: enabled # custom label - need for cleanup job
  name: $namespace
">./environment/namespaces/$namespace.ns.yaml

chmod 775 ./environment/namespaces/$namespace.ns.yaml




####### HELM RELEASE ########
echo "---
  apiVersion: helm.fluxcd.io/v1
  kind: HelmRelease
  metadata:
    name: $helm_release_name
    namespace: $namespace
    annotations:
      fluxcd.io/ignore: \"false\"
      fluxcd.io/automated: \"$fluxcd_automated\"
      fluxcd.io/tag.ad: semver:~0.1
      fluxcd.io/tag.cart: semver:~0.1
      fluxcd.io/tag.checkout: semver:~0.1
      fluxcd.io/tag.currency: semver:~0.1
      fluxcd.io/tag.email: semver:~0.1
      fluxcd.io/tag.frontend: semver:~0.1
      fluxcd.io/tag.loadgenerator: semver:~0.1
      fluxcd.io/tag.payment: semver:~0.1
      fluxcd.io/tag.productcatalog: semver:~0.1
      fluxcd.io/tag.recommendation: semver:~0.1
      fluxcd.io/tag.shipping: semver:~0.1
      fluxcd.io/tag.grafana-load-dashboards: semver:~0.1
  spec:
    releaseName: $helm_release_name
    helmVersion: v3
    chart:
      git: git@gitlab.com:gugalaps/k8s-mvp-flux-env-dev
      ref: master
      path: environment/charts/$namespace
    values:
      ad:
        image:
          repository: neogeowild/adservice
          tag: $image_ad_version
        namespace: $namespace
      cart:
        image:
          repository: neogeowild/cartservice
          tag: $image_cart_version
        namespace: $namespace
        redis:
          cluster:
            enabled: false
          usePassword: false
          global:
            storageClass: standard
      checkout:
        image:
          repository: neogeowild/checkoutservice
          tag: $image_checkout_version
        namespace: $namespace
      currency:
        image:
          repository: neogeowild/currencyservice
          tag: $image_currency_version
        namespace: $namespace
      email:
        image:
          repository: neogeowild/emailservice
          tag: $image_email_version
        namespace: $namespace
      frontend:
        image:
          repository: neogeowild/frontend
          tag: $image_frontend_version
        namespace: $namespace
        ingress:
          host: $host_prefix.$ingress_gateway_ip.nip.io
      loadgenerator:
        image:
          repository: neogeowild/loadgenerator
          tag: $image_loadgenerator_version
        namespace: $namespace
        ingress:
          host: frontend
      payment:
          image:
            repository: neogeowild/paymentservice
            tag: $image_payment_version
          namespace: $namespace
      productcatalog:
        image:
          repository: neogeowild/productcatalogservice
          tag: $image_productcatalog_version
        namespace: $namespace
      recommendation:
        image:
          repository: neogeowild/recommendationservice
          tag: $image_recommendation_version
        namespace: $namespace
      shipping:
        image:
          repository: neogeowild/shippingservice
          tag: $image_shipping_version
        namespace: $namespace
      istio-vservices:
        grafana: grafana.$ingress_gateway_ip.nip.io
        kiali: kiali.$ingress_gateway_ip.nip.io
        prometheus: prometheus.$ingress_gateway_ip.nip.io
        jaeger: jaeger.$ingress_gateway_ip.nip.io
      flagger:
        namespace: $namespace
      grafana-load-dashboards:
        namespace: $namespace">./environment/releases/$namespace.values.yaml

chmod 775 ./environment/releases/$namespace.values.yaml