#!/bin/sh



for f in ./tmp/* ; do
    source $f
    cp $f ./vers/$namespace.sh
    echo '\n#updated: '$(date)>> ./vers/$namespace.sh
done

for f in ./tmp/* ; do
    rm $f
done

for f in ./vers/* ; do
    source $f

    if [ $command = "CREATE" ]; then
        ./scripts/gen-flux-values.sh $namespace \
        $helm_release_name \
        $image_ad_version \
        $image_cart_version \
        $image_checkout_version \
        $image_currency_version \
        $image_email_version \
        $image_frontend_version \
        $image_loadgenerator_version \
        $image_payment_version \
        $image_productcatalog_version \
        $image_recommendation_version \
        $image_shipping_version \
        $manifest_ad_version \
        $manifest_cart_version \
        $manifest_checkout_version \
        $manifest_currency_version \
        $manifest_email_version \
        $manifest_frontend_version \
        $manifest_loadgenerator_version \
        $manifest_payment_version \
        $manifest_productcatalog_version \
        $manifest_recommendation_version \
        $manifest_shipping_version \
        $manifest_grafana_load_dashboards_version \
        $manifest_istio_vservices_version \
        $manifest_flagger_version \
        $fluxcd_automated \
        $host_prefix \
        $ingress_gateway_ip
    fi


    if [ $command = "DELETE" ]; then
        ./scripts/cleanup.sh $namespace
    fi
done