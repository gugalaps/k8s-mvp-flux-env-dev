#!/bin/sh
namespace=bob
helm_release_name=bob
#images:
image_ad_version="0.0.1"
image_cart_version="0.0.1"
image_checkout_version="0.0.1"
image_currency_version="0.0.1"
image_email_version="0.0.1"
image_frontend_version="0.0.1"
image_loadgenerator_version="0.0.1"
image_payment_version="0.0.1"
image_productcatalog_version="0.0.1"
image_recommendation_version="0.0.1"
image_shipping_version="0.0.1"
#manifests:
manifest_ad_version="0.5.0"
manifest_cart_version="0.4.1"
manifest_checkout_version="0.4.0"
manifest_currency_version="0.4.0"
manifest_email_version="0.4.0"
manifest_frontend_version="0.21.0"
manifest_loadgenerator_version="0.4.0"
manifest_payment_version="0.3.0"
manifest_productcatalog_version="0.3.0"
manifest_recommendation_version="0.3.0"
manifest_shipping_version="0.3.0"
manifest_grafana_load_dashboards_version="0.0.3"
manifest_istio_vservices_version="0.1.0"
manifest_flagger_version="0.1.0"
host_prefix=bob
ingress_gateway_ip="34.75.229.24"
#fluxcd.automated: true/false
fluxcd_automated="true"
#command: CREATE/DELETE 
command=CREATE


#updated: четверг, 21 мая 2020 г. 11:36:03 (MSK)
